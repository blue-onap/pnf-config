To launch the services just run `docker-compose up`.

Once OpendDaylight is up and running, you'll have configure it by running:

```
$ ./setup/odl-setup-ks.sh
```

