#!/bin/bash

SCRIPT_PATH=$(dirname $(realpath -s $0))
openssl s_client -connect localhost:6513 -CAfile $SCRIPT_PATH/ca.pem -cert $SCRIPT_PATH/cert.pem -key $SCRIPT_PATH/key.pem
