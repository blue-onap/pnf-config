#!/bin/bash

set -euo pipefail

SCRIPT_PATH=$(dirname $(realpath -s $0))

ODL_URL=http://localhost:8181

http --check-status $ODL_URL/readiness-check

USER=admin
PASSWORD=admin

RESTCONF_URL=$ODL_URL/restconf
sess=.session

http --auth $USER:$PASSWORD --session=$sess --print Hh DELETE $RESTCONF_URL/config/netconf-keystore:keystore
for json in $SCRIPT_PATH/??_ks_*.json; do
    http --check-status --session=$sess --print Hh POST $RESTCONF_URL/config/netconf-keystore:keystore < $json
done
